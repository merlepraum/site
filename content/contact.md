+++
title = "Contact"

description = ""
+++

Feel free to [join us on WhatsApp by clicking here](https://chat.whatsapp.com/EU90IgfnU64LpwNovdMEeC).

We also have a sponsored Zulip chat server for more organised studying, which you can [join by clicking here](https://maasmath.zulipchat.com/).

To get in touch otherwise, pop us an email at hello at maasmath.eu

<p></p>
