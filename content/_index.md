+++
title = "About"

description = ""
+++
We are a community of Maastricht University students with a deep appreciation for the
highest domain of human intellect &ndash; Mathematics.

The goal of our little collective is to help one another learn the field by
grassroots study sessions and group readings. In addition, we like to share bits of
glittery Math goodness that pique our interest among like minded people.

If this sounds like something you'd be interested in, don't be afraid to [join our
WhatsApp](/contact). We originated as a group of DKE students, but all those who are
interested are welcome to join. Formally, we are a [Special Interest Group (SIG) of
MSV Incognito](https://msvincognito.nl/members/).

This website is hosted on [GitLab](https://gitlab.com/maasmath/site), and uses the
lovely [Playfair Display](https://github.com/clauseggers/Playfair-Display) and [Libre
Baskerville](https://github.com/impallari/Libre-Baskerville) fonts.
