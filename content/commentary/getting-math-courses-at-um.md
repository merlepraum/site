+++
title = "Math courses at UM"
date = 2020-05-03
description = "A brief rundown of where to look for them"
author = "Chris"
+++
So, if you're looking to take more mathy courses at UM outside your programme there
is some hope for you. In general the procedure is to first find a course and then to
kindly ask your department to let you take it. How feasible this is depends on your
faculty of course, so keep in mind this post is written from the perspective of a DKE
student.

## Where to find them?
Your best bets are to browse the catalogues of:
 - [Econometrics
   (Course code EBC)](https://www.maastrichtuniversity.nl/education/bachelor/bachelor-econometrics-and-operations-research/courses-curriculum)
 - [Data Science and Knowledge Engineering
   (Course code KEN)](https://www.maastrichtuniversity.nl/education/bachelor/bachelor-data-science-and-knowledge-engineering/courses-curriculum)
 - [Maastricht Science Programme
   (Course code MAT, or maybe PHY)](https://www.maastrichtuniversity.nl/education/bachelor/bachelor-maastricht-science-programme/courses-curriculum)

If you don't find what you want, there is still a very slight chance you might find
it in some other programme using the [course
finder](https://www.maastrichtuniversity.nl/education/course-finder).

## Which course to pick?

Ultimately, your search space isn't so great. However, some courses between
programmes overlap, for example Calculus at DKE, MSP and Analysis 1 at SBE. In these
cases, there are some rules of thumb; keep in mind they're mostly based on anecdotal
evidence of me and friends:
 - Econometrics will probably be the most rigorous from a theorem-proof perspective.
   Also, it is worth noting some of their courses take an entire semester, even
   though on the website they're listed as just 1 period.
 - DKE will likely be on the practical applications side and only skim (if not skip)
   proofs.
 - MSP courses will have a more introductory approach due to the diverse background
   of its students.

Editor's suggestions are to take Analysis 1 or 2 from Econometrics, as they may be
the nicest way to get acquainted with mathematical proofs _(note that the editor has
a heavy Analysis bias)_.

## Getting the courses (DKE perspective)
Unfortunately I cannot speak about the procedure for getting courses for students
from other faculties.

At DKE, the regulations permit up to 18 ECTS points from other faculties. The easiest
case is if the courses you wish to take are in the first semester - you can simply
grab them as electives in your final year. If they're second semester, you can still
do them, but you will most likely need to do them parallel to your DKE courses.

The procedure is roughly as follows:
 - You sketch up a plan (a list) of all electives and external courses you wish to
   take, with their descriptions and your motivation for taking them. Note that this
   should include electives you take from DKE as well. Make sure to also include
   information as to when you'd like to do them, that is in which period/which year
   of study.
 - You send an email with your wishlist to the DKE Board of Examiners
 - If you get the permission, remember to register for the course in the student
   portal in time

If you have any questions or concerns, you can always reach out to our very lovely
study advisors. They have been extremely helpful from my and my friends' experiences.

### Exchange
In general, one way to hack the system is to go on exchange. However, my personal
search for math courses at partner universities of DKE failed to be very fruitful.
You can always go for a university outside of the list, but you'd need to pay for
your own tuition.

### ...maybe a minor?
Now, having said all that, you may feel a bit disappointed... Is this it? Is there no
other way to have some more formally certified math education?

Well, there is one possibly rather crazy idea which may or may not work. It's not
uncommon in the Netherlands to do minors in third year at universities other than
your own.  Maastricht is a bit secluded, so this doesn't happen a lot here - from my
conversations with study advisors, I heard DKE used to do that in the past but
haven't done it recently.

Some universities like VU or TU/e just offer straight up math education and
thus obviously have a wider catalogue than us. Unless you have something else keeping
you here like KE@Work, it might be worth drilling into this as an option.
