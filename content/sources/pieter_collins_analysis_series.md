+++
title = "Pieter Collins lectures on topology, real analysis, measure theory, and computable analysis"

description = "Series of talks from 2nd semester of 2020/2021"

date = 2021-06-24

extra.type = "lecture"
+++

This is an (ongoing) series of lectures in pure mathematics given by [Dr. Pieter Collins](https://www.maastrichtuniversity.nl/pieter.collins) in the second semester of 2020/2021.

## A squishy (so definitely not very hard) introduction to General Topology

_Held on March 10th 2021._

Topology (based on the Greek word "τόπος" [topos] meaning "place" or "location") is the mathematical study of squishy things - objects that can be stretched, squeezed, twisted or bent, but not torn-apart or smashed into infinitely-many tiny little pieces. (Unlike geometry, which is the study of hard things.)


In this talk I'll introduce the basic topological notions, including continuity, convergence, dimension, boundary, connectivity and compactness (a kind of smallness for infinite objects), which form the foundations of continuous mathematics. It turns out that pure topological formulations (such as for continuity) are often much simpler than the equivalent definitions from calculus. I'll also explain the logical in "topological", and how continuity relates to computation (though we really need a sober space for this).

I'll illustrate the concepts though familiar examples like the real numbers and n-dimensional space (along the way showing that three-dimensional  space really is bigger than two-dimensions, despite them having the same number of elements), and some unfamiliar examples - topology is full of totally unintuitively weird and crazy objects, like mind-bending spaces where sequences can have infinitely many limits.

There won't be time to cover "Algebraic Topology", which allows one to distinguish a doughnut from a pretzel, tie unbreakable knots, comb tennis balls or even find the Higgs boson in a quantum field, but the material here is a crucial prerequisite for these topics.

<video width="700" controls poster="/pieter_collins_analysis_series_banners/topology.png">
  <source src="https://nx17402.your-storageshare.de/s/Zc5pyiHosTWpmJZ/download" type="video/mp4">
Your browser does not support the video tag. See the direct download link below.
</video> 

Time locations:
```
00:00 Introduction
06:15 Boundaries, open sets and closed sets
28:43 Axioms of a topological space
32:39 Examples of topological spaces
53:35 Basis for a topology
59:18 Convergence in topological spaces
1:08:15 Interlude - Break with discussions on: intersection of topology and measure theory, chaos theory, index theory
1:16:52 Brief recap
1:18:19 Continuity
1:26:40 Continuity and convergence
1:29:44 Homeomorphisms - Topological equivalences
1:47:10 Compactness
2:06:34 Finish and questions
```

Direct download link: [here](https://nx17402.your-storageshare.de/s/Zc5pyiHosTWpmJZ/download)

The funky images in the video thumbnail above are:
 - [The Topologist's sine curve](https://en.wikipedia.org/wiki/Topologist%27s_sine_curve)
 - [A cow homeomorphic to a sphere by Keenan Crane](https://www.cs.cmu.edu/~kmcrane/Projects/ModelRepository/)

## Real Analysis: A Really Useful Subject with No Imaginary Parts!

_Held on April 21st 2021_

Real analysis forms the rigorous foundation of calculus and numerical mathematics, and concerns the real numbers and real-valued sequences and functions.

The real numbers themselves are often seen as a mathematical model of "points on the line". I'll start by saying why the rational number are not a good model for the points on a line, and give some properties we want our real numbers to have: a "complete ordered field". I'll then give two equivalent ways of constructing the real numbers by "completing" the rationals, either by Dedekind cuts or Cauchy sequences.

While the real numbers are a remarkably natural and well-behaved mathematical object, real-valued functions are anything but! There are many important spaces of real-valued functions, and different notions of convergence and approximation.

It is easy to find sequences of continuous functions which converge (pointwise) to a discontinuous function; under what conditions is the limit continuous? It turns out that the limit is continuous if the convergence is uniform. Further, any sequence of uniformly bounded and equicontinuous functions has a uniformly convergent subsequence. This latter result is known as Arzelà-Ascoli theorem, and shows that spaces of equicontinuous functions are locally-compact. This is useful in data science, since over-fitting can be avoided by working with a locally-compact set of functions!

Uniform approximation a strong and useful notion of approximation of functions.
By the Weierstrass approximation theorem, any continuous function is locally-uniformly approximable by polynomials, which are about as nice a class of functions as one could hope for, and can be easily manipulated computationally.

A weaker notion of approximation is with respect to integrals of (squared) errors.
We shall show that square-integrable functions are well-approximated by their Fourier series, which has numerous applications from audio signal processing to quantum theory.
By taking completions with respect to (square) integrals we obtain the integrable functions, a class of discontinuous functions closely related to measure theory (which will be the subject of the next talk).

During the talk, I will also develop some basic theory of metric spaces, which are topological spaces with a notion of "distance", and Banach spaces, which are infinite-dimensional complete normed vector spaces, as these provide a natural abstract setting for real analysis.

<video width="700" controls poster="/pieter_collins_analysis_series_banners/real_analysis.png">
  <source src="https://nx17402.your-storageshare.de/s/sHzoAD6R9i9giBL/download" type="video/mp4">
Your browser does not support the video tag. See the direct download link below.
</video> 

Time locations:
```
00:00 Introduction
01:40 Field axioms
07:34 Constructions of the Reals by Dedekind cuts
24:39 Constructions of the Reals by equivalence classes of Cauchy sequences
40:17 Metric spaces
46:23 Real functions
51:14 Pointwise and uniform convergence
01:05:50 Arzelà–Ascoli theorem
01:29:05 Weierstrass Approximation Theorem
01:43:30 Sequential convergence as criterion for uniform convergence
01:44:48 Banach spaces
01:50:00 Fourier Analysis and links to Measure Theory
```

Direct download link: [here](https://nx17402.your-storageshare.de/s/sHzoAD6R9i9giBL/download)

The images in the video thumbnail are:
 - [An illustration of Dedekind cuts used to construct the real numbers from the rationals](https://en.wikipedia.org/wiki/Construction_of_the_real_numbers#Construction_by_Dedekind_cuts)
 - [An example of a pointwise convergent series of continuous functions with a discontinuous limit, showing why uniform convergence is nicer](https://en.wikipedia.org/wiki/Uniform_limit_theorem)

## How much Measure Theory is good for you?

_Held on May 19th 2021_

Measuring and weighing goods has been a fundamental part of human economic activity for millenia. The Weighhouse ("Waag") is an important landmark of many Dutch towns, and tourists come from the world over to see Dutch cheeses being weighed at the Waag in Alkmaar. Unlike counting, which is concerned with discrete, often indivisible entities, measuring relates to a continuum scale (quantum effects nonwithstanding). The mathematics of measuring is therefore fundamentally related to the real numbers.

A measure on a set is a way of assigning a "size" (length, area, volume ...) to its subsets. I'll start by looking at the most important example of a measure: the standard Lebesgue measure on the real line, for which the measure of an interval [a,b] is its length b-a. But what about the measure of more complicated sets, like the set of rational numbers? It turns out we can assign a reasonable measure to much more complicated sets, including all Borel sets, which contain the open sets and much more besides.

Is every set measurable? By using the contentious Axiom of Choice, we can construct non-measurable subsets, and even dissect a ball into pieces which reassemble into two balls, each of the same volume! But it's also possible to develop mathematics so that ensure that every set is measurable, as long as we only want Countable Choice. What variant of mathematics best matches your own intuition?

I'll next look at the properties an abstract measure have in order to reasonably capture the notion of size, including the algebraic structure of the measurable sets. We'll look at some examples of measures (and probability distributions), including point-measures, product measures on product spaces, and absolutely-continuous measures, which are defined by integrals.

This leads nicely to one of the main tools of measure theory, the Lebesgue integral. This extends the notion of integration beyond the piecewise-continuous functions handled by the Riemann integral to cover the class of measurable functions. Fubini's theorem shows that an integral in two-dimensions can be computed by two one-dimensional integrals. The integral defines a linear functional on the vector space of continuous functions, and by the Riesz-Markov-Kakutani representation theorem, any positive such functional is an integral with respect to a measure. This idea also yields a natural topology, that of weak convergence, for which there are many equivalent definitions by the "portmanteau theorem". Further, (for reasonable spaces X) the "weak topology" is actually a weak-* topology, so by the Banach-Alaoglu theorem, the closed unit ball in the space of measures is compact.

The most important application of measure theory, as realised by Kolmogorov, is as a setting for probability theory. Probability distributions are modelled by measures, random variables by measurable functions, and expectations (averages) by integrals. Throughout the talk, I'll illustrate how ideas of measure theory can be used to talk about notions of chance.

<video width="700" controls poster="/pieter_collins_analysis_series_banners/measure_theory.png">
  <source src="https://nx17402.your-storageshare.de/s/dTnmLQACRJQ5RYc/download" type="video/mp4">
Your browser does not support the video tag. See the direct download link below.
</video> 

Time locations:
```
00:00 Background
04:14 Introduction
14:20 Properties of measures
25:40 (Borel) measurable sets
29:45 Sigma-algebras
34:20 How to define measurable sets - outer measures and Carathéodory's extension theorem
45:00 Measure of countable sets
49:05 Measure and probability
53:07 Are all (bounded) subsets of R Lebesgue measurable?
58:45 Interlude - Break with discussions on: what the Axiom of Choice has to do with computability, Axiom of Countable Choice
1:10:58 Other measures
1:20:50 Measurable functions
1:23:15 Integration - integrals on measures, Lebesgue integral and how it differs from Riemann integrals
1:35:30 Properties of integration
1:39:37 Riesz Representation Theorem
1:44:45 Topology on measures - The Portmanteau Theorem
1:55:10 Properties of the weak topology on measures
2:00:44 More on probability - how integrals give expectations, and measurable functions give random variables
2:02:45 Finish and questions
```

Direct download link: [here](https://nx17402.your-storageshare.de/s/dTnmLQACRJQ5RYc/download)

The images in the video thumbnail are:
 - [An illustrative comparison of the Riemann and Lebesgue integrals](https://en.wikipedia.org/wiki/Lebesgue_integration#Intuitive_interpretation)
 - [A visualization of the Banach-Tarski paradox, where by simply moving and rotating pieces of a ball around, it is possible to arrange them in such a way that we get two balls of volume equal to the first one](https://en.wikipedia.org/wiki/Banach%E2%80%93Tarski_paradox)

## Computer Says: Know Your Error Bounds - An introduction to Computable Analysis

_Held on June 14th 2021_

In scientific computing, most software computes numerical approximations to the correct mathematical answer. Even simple calculations yield small errors (just try 0.6+0.3+0.1-1 in your favourite environment). For more complicated applications, there are many, many sources of these errors, and they might just add up to give a big error... and worse, we wouldn't even know our answer was wrong! Surely we can do better?

Well computers can do a lot, but they can't do everything - solving the "halting problem" is an example of a task which has been proven to be impossible. The study of what digital computers can do, and more interestingly, what they can't do, is called computability theory, and is very well developed for discrete mathematical problems.

Computability theory for continuous mathematics is complicated by the fact that objects typically lie in uncountable spaces, and so general objects require an infinite amount of data to describe. For example, the real numbers can be described by their infinite decimal expansions. (Note that some numbers, even irrational numbers such e=2.71828⋅⋅⋅ can still be described in other ways with a finite amount of data, e.g. e=lim[n->infty](1+1/n)^n, but for almost all numbers, there's no finite description of any kind.) This means that when formalising computation in analysis, we need to work on infinite streams of data. Since there's not enough time in the universe to wait for such a computation to finish, we need to be able to extract meaningful information from a finite prefix of the stream, typically an open set containing the object. The resulting theory is therefore highly topological in nature, and the most fundamental theorem is that only continuous functions can be computable.

Even once we have set up the basic theory, there are still a few surprises... Suppose we try to compute 3×0.66666⋅⋅⋅. What is the first digit of the answer? This means that the basic decimal expansion cannot be used to represent the real numbers. Fortunately, there are many suitable representations of the real numbers, including signed-digit expansions, and Dedekind and Cauchy representations based on the order and metric structure. However, comparisons > and ≥ on ℝ cannot be decided when the values to be compared are equal (is 2<3×0.66666⋅⋅⋅?), so instead, comparisons return a Kleenean data type, which has a third value "indeterminate" for answers which are so sensitive as to be unknowable.

Once we have types representing the real number and Kleenean logic, we can easily construct more complicated objects. There are general constructions of product types X×Y and types of continuous functions X→Y, which means that the objects we can work with form a "Cartesian closed category", and model the well-known lambda-calculus and intuitionistic type theory. In particular, there are simple constructions types of open, closed, and compact sets, as well as a type of "overt" sets. Further, most results from topology and analysis carry over to this setting, notably that naturally-defined continuous operators are usually computable. An important possible exception is the countable product of compact sets, which is continuous by Tychonoff's theorem, but for which no computability proof is known.

Measure theory and probability are trickier. The basic concepts of sigma-algebra and measurable function are highly non-computable, so the standard theory cannot be used. In the last part of the talk, I will discuss a computable approach to probability and random variables which I recently developed using "lower-measurable sets".

<video width="700" controls poster="/pieter_collins_analysis_series_banners/computable_analysis.png">
  <source src="https://nx17402.your-storageshare.de/s/rzEXsHGLRYZ9MqJ/download" type="video/mp4">
Your browser does not support the video tag. See the direct download link below.
</video> 

```
00:00 Background - literature, computability
19:15 Introduction
34:00 Basic theory of computable analysis
52:25 Break with brief discussion of functional data analysis
1:02:07 Topology of computable analysis
1:26:40 Real number representations
1:36:50 Computability of real arithmetic
1:45:40 General constructions
2:06:58 Computable topology
2:21:10 Finish and questions
```

Direct download link: [here](https://nx17402.your-storageshare.de/s/rzEXsHGLRYZ9MqJ/download)

[The little table in the video thumbnail shows a negation of a Kleenean data type, which can be true, false or unknown.](https://commons.wikimedia.org/wiki/File:Kleene_3-valued_logic.png)
