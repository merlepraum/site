+++
title = "Sources"

description = ""

template = "sources.html"

insert_anchor_links = "left"

extra.content_lectures = """
We organised a few lectures which you might find interesting:
"""
extra.content_links = """
Beyond that, we have stumbled upon quite a few handy online resources for self-study:

 - [Oxford University provides resources like notes for free for all mathematics BSc courses they offer!](https://courses.maths.ox.ac.uk/overview/undergraduate)
 - [This seemingly obscure list is a solid guide to books on basically any math undergrad topic.](http://freecomputerbooks.com/puremath.htm)
 - [Unofficial notes by a Cambridge math undergrad.](https://dec41.user.srcf.net/notes/)

"""
extra.content_books = """
Furthermore, here are a couple of books we found and tried:
"""
+++
Note: The content below +++ is not rendered, because this particular page uses the `sources.html` template. It's a bit hacky, but to modify the text you need to modify the `extra.content_*` bits above.
