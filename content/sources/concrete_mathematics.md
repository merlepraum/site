+++
title = "Concrete Mathematics"

description = "Number theory and Mathematics for Computer Science"

extra.type = "book"
+++
Following the suggestion of one of our professors, we read "Concrete Mathematics" by
Graham, Knuth and Patashnik. The book is based on old Stanford courses in Mathematics
for Computer Science.

We chose it to supplement and expand on our Discrete Mathematics course of first year
DKE. A notable topic introduced in the book which is otherwise missing from the DKE
curriculum is number theory, a necessary prerequisite for anyone with an interest in
cryptography.

It makes for a brilliant reading and is much recommended to go through rigorously. 
Given time constraints of a single block, we chose to study only selected chapters
collaboratively, yet we discourage future readers from such an approach. Most pages
are filled to the brim with worthwhile content and skipping any results in nothing
other than confusion and holes in rudimentary knowledge.
